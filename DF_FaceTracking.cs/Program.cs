﻿/*******************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2013 Intel Corporation. All Rights Reserved.

*******************************************************************************/

using System;
using System.Windows.Forms;

namespace DF_FaceTracking.cs
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Create Session
            PXCMSession session = PXCMSession.CreateInstance();

            //If Session creation was successfull, create SenseManager, FaceModule and configuration
            if (session != null)
            {
                //Create SenseManager and initialize it
                PXCMSenseManager senseManager = PXCMSenseManager.CreateInstance();
                senseManager.EnableFace();
                senseManager.Init();

                //Create FaceModule
                PXCMFaceModule faceModule = senseManager.QueryFace();
                faceModule = senseManager.QueryFace();

                //Create Configuration
                PXCMFaceConfiguration cfg = faceModule.CreateActiveConfiguration();
                PXCMFaceConfiguration.ExpressionsConfiguration expc = cfg.QueryExpressions();
                expc.EnableAllExpressions();
                expc.Enable();
                cfg.ApplyChanges();

                //Get the number of faces each frame
                while (senseManager.AcquireFrame(true).IsSuccessful())
                {
                    //Get number of faces
                    PXCMFaceData faceData = faceModule.CreateOutput();
                    faceData.Update();
                    Int32 nfaces = faceData.QueryNumberOfDetectedFaces();
                    //Console.WriteLine(nfaces);

                    //For each face get the pose and expression data
                    for (int i = 0; i < nfaces; i++)
                    {
                        PXCMFaceData.Face face = faceData.QueryFaceByIndex(i);
                        //retrieve the pose information
                        PXCMFaceData.PoseData pdata = face.QueryPose();
                        PXCMFaceData.PoseEulerAngles angles;
                        pdata.QueryPoseAngles(out angles);
                        Console.WriteLine(angles.pitch);
                        Console.WriteLine(angles.roll);
                        Console.WriteLine(angles.yaw);

                        //retrieve the expression information
                        PXCMFaceData.ExpressionsData edata = face.QueryExpressions();
                        if (edata != null)
                        {
                            PXCMFaceData.ExpressionsData.FaceExpressionResult leftEye;
                            edata.QueryExpression(PXCMFaceData.ExpressionsData.FaceExpression.EXPRESSION_EYES_CLOSED_LEFT, out leftEye);
                            Console.WriteLine(leftEye.intensity);
                            PXCMFaceData.ExpressionsData.FaceExpressionResult rightEye;
                            edata.QueryExpression(PXCMFaceData.ExpressionsData.FaceExpression.EXPRESSION_EYES_CLOSED_RIGHT, out rightEye);
                            Console.WriteLine(rightEye.intensity);
                            bool eyesClosed = false;
                            int x = 0;
                            if (leftEye.intensity > 75 && rightEye.intensity > 75)
                            {
                                eyesClosed = true;
                            }
                            Console.WriteLine(eyesClosed);
                        }
                    }
                    senseManager.ReleaseFrame();
                }
                senseManager.Dispose();
                session.Dispose();
            }
        }
    }
}

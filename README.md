# realsense-facetracking

This program is just for presentation reasons.

*  In order to get this c# project running, you need to have the RealSense 2016 R2 SDK installed on your system. 
You can download it from [here](https://software.intel.com/en-us/realsense-sdk-windows-eol) (at the bottom of the page).

*  Of course you also need to plug in the RealSense R300 Camera. 


*  Just open the c# Solution in Visual Studio. In theory it should work right away. 

For now there is a visualization of the live video augmented with the head pose data (yaw, pitch, roll) as well as indicators for the left and right eye state. 

![](https://imgur.com/e43McmX.jpg)
